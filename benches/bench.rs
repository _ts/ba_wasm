#![feature(test)]

extern crate test;

use std::net::TcpStream;
use test::Bencher;
use std::io::{Write, Read};
use config::ADDRESS;

#[bench]
fn bench_round_time(b: &mut Bencher) {
    let mut server = TcpStream::connect(ADDRESS).unwrap();
    let data = "Hello!";

    b.iter(|| {
        let mut buf = vec![0; 128];
        server.write(&data.as_bytes()).unwrap();

        server.read(&mut buf).unwrap();
    });
}
