use anyhow::Result;

use wasi_net_bindings;
use config::ALIAS;

// simple echo client
fn looper() -> Result<()> {
    let mut handle = wasi_net_bindings::NetHandle::new(ALIAS);
    handle.open()?;

    loop {
        let mut buf = vec![0; 128];
        let len = handle.read(&mut buf)?;
        handle.write(&buf[..len])?;
    }

    handle.close()?;

    Ok(())
}

#[no_mangle]
fn _start() {
    looper().unwrap()
}

// required
fn main() {}