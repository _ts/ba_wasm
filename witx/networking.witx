(typename $bool
    (enum u8
        $false
        $true))

;;; A raw byte buffer.
(typename $net_buf (array u8))

(typename $net_errno
    (enum u32
        $ok
        $end_of_file
        ;;; The connection was not allowed with the given alias.
        $alias_not_found
        ;;; This usually indicates a closed connection. Call open again to connect to a new peer.
        $peer_not_found
        $other))

(typename $net_config
    (struct
        (field $max_buf_len u32)))

;;; Any connection within this module is stateless and is handled through its alias and peer id.
(module $networking
    ;;; Returns the general configuration, which must be adhered by the guest.
    (@interface func (export "net_config")
        (result $error $net_errno)
        (result $cfg $net_config)
    )

    ;;; Tries to open a connection with the given alias. Multiple calls to net_open will never fail.
    ;;; Returns the id of the peer.
    (@interface func (export "net_open")
        (param $alias string)
        (result $error $net_errno)
        (result $peer u64)
    )

    ;;; Read raw bytes from the network with the specified alias.
    ;;; If buf is empty, this call will block until a connection was received (e.g. accepting client connections).
    ;;; Returns the bytes read into buf.
    (@interface func (export "net_read")
        (param $alias string)
        (param $buf $net_buf)
        (param $peer u64)
        (result $error $net_errno)
        (result $len u32)
    )

    ;;; Write raw bytes to the connection identified by alias.
    ;;; Returns the bytes written out of buf.
    (@interface func (export "net_write")
        (param $alias string)
        (param $buf $net_buf)
        (param $peer u64)
        (result $error $net_errno)
        (result $len u32)
    )

    ;;; Prematurely close the connection identified by its alias and peer id.
    (@interface func (export "net_close")
        (param $alias string)
        (param $peer u64)
        (result $error $net_errno)
    )
)
