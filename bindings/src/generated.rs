// This file is automatically generated, DO NOT EDIT
//
// To regenerate this file run the `crates/witx-bindgen` command

use core::mem::MaybeUninit;

pub use crate::error::Error;
pub type Result<T, E = Error> = core::result::Result<T, E>;
pub type Bool = u8;
pub const BOOL_FALSE: Bool = 0;
pub const BOOL_TRUE: Bool = 1;
pub type NetBuf<'a> = &'a [u8];
pub type NetErrno = u32;
pub const NET_ERRNO_OK: NetErrno = 0;
pub const NET_ERRNO_END_OF_FILE: NetErrno = 1;
/// The connection was not allowed with the given alias.
pub const NET_ERRNO_ALIAS_NOT_FOUND: NetErrno = 2;
/// This usually indicates a closed connection. Call open again to connect to a new peer.
pub const NET_ERRNO_PEER_NOT_FOUND: NetErrno = 3;
pub const NET_ERRNO_OTHER: NetErrno = 4;
#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct NetConfig {
    pub max_buf_len: u32,
}
/// Returns the general configuration, which must be adhered by the guest.
pub unsafe fn net_config() -> Result<NetConfig> {
    let mut cfg = MaybeUninit::uninit();
    let rc = networking::net_config(cfg.as_mut_ptr());
    if let Some(err) = Error::from_raw_error(rc) {
        Err(err)
    } else {
        Ok(cfg.assume_init())
    }
}

/// Tries to open a connection with the given alias. Multiple calls to net_open will never fail.
/// Returns the id of the peer.
pub unsafe fn net_open(alias: &str) -> Result<u64> {
    let mut peer = MaybeUninit::uninit();
    let rc = networking::net_open(alias.as_ptr(), alias.len(), peer.as_mut_ptr());
    if let Some(err) = Error::from_raw_error(rc) {
        Err(err)
    } else {
        Ok(peer.assume_init())
    }
}

/// Read raw bytes from the network with the specified alias.
/// If buf is empty, this call will block until a connection was received (e.g. accepting client connections).
/// Returns the bytes read into buf.
pub unsafe fn net_read(alias: &str, buf: NetBuf<'_>, peer: u64) -> Result<u32> {
    let mut len = MaybeUninit::uninit();
    let rc = networking::net_read(
        alias.as_ptr(),
        alias.len(),
        buf.as_ptr(),
        buf.len(),
        peer,
        len.as_mut_ptr(),
    );
    if let Some(err) = Error::from_raw_error(rc) {
        Err(err)
    } else {
        Ok(len.assume_init())
    }
}

/// Write raw bytes to the connection identified by alias.
/// Returns the bytes written out of buf.
pub unsafe fn net_write(alias: &str, buf: NetBuf<'_>, peer: u64) -> Result<u32> {
    let mut len = MaybeUninit::uninit();
    let rc = networking::net_write(
        alias.as_ptr(),
        alias.len(),
        buf.as_ptr(),
        buf.len(),
        peer,
        len.as_mut_ptr(),
    );
    if let Some(err) = Error::from_raw_error(rc) {
        Err(err)
    } else {
        Ok(len.assume_init())
    }
}

/// Prematurely close the connection identified by its alias and peer id.
pub unsafe fn net_close(alias: &str, peer: u64) -> Result<()> {
    let rc = networking::net_close(alias.as_ptr(), alias.len(), peer);
    if let Some(err) = Error::from_raw_error(rc) {
        Err(err)
    } else {
        Ok(())
    }
}

pub mod networking {
    use super::*;
    #[link(wasm_import_module = "networking")]
    extern "C" {
        /// Returns the general configuration, which must be adhered by the guest.
        pub fn net_config(cfg: *mut NetConfig) -> NetErrno;
        /// Tries to open a connection with the given alias. Multiple calls to net_open will never fail.
        /// Returns the id of the peer.
        pub fn net_open(alias_ptr: *const u8, alias_len: usize, peer: *mut u64) -> NetErrno;
        /// Read raw bytes from the network with the specified alias.
        /// If buf is empty, this call will block until a connection was received (e.g. accepting client connections).
        /// Returns the bytes read into buf.
        pub fn net_read(
            alias_ptr: *const u8,
            alias_len: usize,
            buf_ptr: *const u8,
            buf_len: usize,
            peer: u64,
            len: *mut u32,
        ) -> NetErrno;
        /// Write raw bytes to the connection identified by alias.
        /// Returns the bytes written out of buf.
        pub fn net_write(
            alias_ptr: *const u8,
            alias_len: usize,
            buf_ptr: *const u8,
            buf_len: usize,
            peer: u64,
            len: *mut u32,
        ) -> NetErrno;
        /// Prematurely close the connection identified by its alias and peer id.
        pub fn net_close(alias_ptr: *const u8, alias_len: usize, peer: u64) -> NetErrno;
    }
}
