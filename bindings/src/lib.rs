use std::io::{Read, Write};

pub use generated::*;

mod error;
mod generated;

pub struct NetHandle {
    alias: String,
    peer: Option<u64>,
}

impl NetHandle {
    pub fn new(alias: &str) -> Self {
        NetHandle {
            alias: alias.to_string(),
            peer: None,
        }
    }

    pub fn open(&mut self) -> Result<u64> {
        unsafe {
            let id = net_open(&self.alias)?;
            self.peer.replace(id);
            Ok(id)
        }
    }

    pub fn close(&mut self) -> Result<()> {
        match self.peer {
            None => Err(Error::Closed),
            Some(p) => {
                unsafe {
                    net_close(&self.alias, p)?;
                }
                self.peer.take();
                Ok(())
            }
        }
    }

    pub fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        match self.peer {
            None => Err(Error::Closed),
            Some(p) => {
                unsafe {
                    let len = net_read(&self.alias, buf, p)?;
                    Ok(len as usize)
                }
            }
        }
    }

    pub fn write(&mut self, buf: &[u8]) -> Result<usize> {
        match self.peer {
            None => Err(Error::Closed),
            Some(p) => {
                unsafe {
                    let len = net_write(&self.alias, buf, p)?;
                    Ok(len as usize)
                }
            }
        }
    }
}

impl Read for NetHandle {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        Ok(self.read(buf)?)
    }
}

impl Write for NetHandle {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        Ok(self.write(buf)?)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

impl Drop for NetHandle {
    fn drop(&mut self) {
        let _ = self.close();
    }
}