use core::fmt;

use super::generated::*;
use std::io::ErrorKind;

pub enum Error {
    EndOfFile,
    Closed,
    AliasNotFound,
    PeerNotFound,
    Other,
}

impl Error {
    pub fn from_raw_error(error: NetErrno) -> Option<Error> {
        match error {
            NET_ERRNO_OK => None,
            NET_ERRNO_END_OF_FILE => Some(Error::EndOfFile),
            NET_ERRNO_ALIAS_NOT_FOUND => Some(Error::AliasNotFound),
            NET_ERRNO_PEER_NOT_FOUND => Some(Error::PeerNotFound),
            NET_ERRNO_OTHER |
            _ => Some(Error::Other)
        }
    }

}

impl From<Error> for std::io::Error {
    fn from(_: Error) -> Self {
        ErrorKind::Other.into()
    }
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "error {}", self)?;
        Ok(())
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "error {}", self)?;
        Ok(())
    }
}
