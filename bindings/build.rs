use std::fs;

use witx_bindgen;

fn main() -> std::io::Result<()> {
    fs::write("./src/generated.rs", witx_bindgen::generate(&["../witx/networking.witx"]))?;
    Ok(())
}
