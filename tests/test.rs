#![feature(test)]

use std::net::{TcpStream, Shutdown};
use std::io::{Read, Write};
use config::ADDRESS;

#[test]
fn test_simple_echo() {
    let mut server = TcpStream::connect(ADDRESS).unwrap();

    for i in 1..99 {
        server.write(&i.to_string().as_bytes()).unwrap();

        let mut buf = vec![0; 128];
        let len = server.read(&mut buf).unwrap();

        println!("Received Buffer: {} -- With Length {}", String::from_utf8_lossy(&buf[..len]), len);

        let recv_i = String::from_utf8_lossy(&buf[..len]).parse::<i32>().unwrap();

        assert_eq!(i, recv_i);
    }

    server.shutdown(Shutdown::Both).unwrap();
}
