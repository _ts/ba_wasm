# WASI Network Bindings

This project is part of a bachelor thesis at the HTW Berlin and is done in cooperation with the gematik GmbH.
The goal is to provide a solution to the missing network implementation within WASI.
In contrast to WASI this implementation does not adhere to any POSIX standard and is more of an abstract
view on modern networking with respect to the sandbox mechanism of WebAssembly.
Therefore, only a handful of commands is provided:

- `net_read(alias, buf, peer_id) -> bytes_read`
- `net_write(alias, buf, peer_id) -> bytes_written`
- `net_close(alias, peer_id)`
- `net_open(alias) -> peer_id`
- `net_config() -> config`

All of these functions above are stateless; any communication is handled with a combination of `alias` and `peer_id`.
The `alias` reflects an actual implementation detail on the host side, where `peer_id` is a representation of a remote peer.
As an example, a TCP server with `alias = "some_alias"` will serve several `peer_id`s.

The `net_config` function returns a struct with several constant configurations. Currently, this only includes the maximum buffer size
which is consumed by the host once at a call to write or read.

## Structure

The `wasi-net-cli` crate includes a self-contained `wasmtime` runtime which implements the host side bindings and runs the
`wasi-net-guest` (`guest` dir) example; again the `wasi-net-bindings` crate is a library implementing the guest side bindings.

`witx/networking` is the WITX specification used by the above.

## Getting Started

To run the `wasi-net-cli` the Rust nightly toolchain is required.
For instructions refer to https://doc.rust-lang.org/1.2.0/book/nightly-rust.html.

```bash
cd wasi-net-cli # root dir of this project
cd guest
cargo build # builds the echo server as .wasm file
cd ..
cargo run
```

The default address is `127.0.0.1:6789`. To change this go to the `config` crate within the directory of the same name.

Now execute the test located in `tests/test.rs` to see the simple echo client example in action:

```bash
cargo test --test test test_simple_echo -- --nocapture
```

Depending on the system this is running, `wasi-net-cli` will crash with the following two errors:
```
Host error: EndOfFile
Host error: PeerNotFound
```
The peer disconnected and now the guest will receive an error indicating this with `EndOfFile`.
The second error originates from the `Drop` trait implemented by `wasi-net-bindings` and can be ignored.
A sturdier version of the echo server would handle the two errors above to connect to a new client.



