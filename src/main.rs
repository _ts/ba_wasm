#![feature(box_syntax)]

use anyhow::Result;
use wasmtime::{Linker, Module, Store};

use config::{ALIAS, URI};

use crate::ctx::WasiNetCtx;
use crate::handle::{NetCapabilities, TcpListenerUriHandle, UriHandle};

mod witx;
mod ctx;
mod handle;

wasmtime_wiggle::wasmtime_integration!({
    target: witx,
    witx: ["witx/networking.witx"],
    ctx: WasiNetCtx,
    modules: {
        networking => {
          name: WasiNet,
          docs: "",
          function_override: {}
        }
    }
});

fn main() -> Result<()> {
    let store = Store::default();
    let mut linker = Linker::new(&store);

    let handles: Vec<(String, Box<dyn UriHandle>)> = vec![(ALIAS.to_string(), box TcpListenerUriHandle::new(URI, NetCapabilities {
        listen: true,
        read: true,
        write: true,
    })?)];

    let wasi_net = WasiNet::new(&store, WasiNetCtx::new(handles, None));
    wasi_net.add_to_linker(&mut linker)?;

    let module = Module::from_file(store.engine(), "guest/target/wasm32-unknown-unknown/debug/wasi-net-guest.wasm")?;
    linker.module("", &module)?;

    // retrieve the _start() function
    let main = linker.get_default("")?;
    main.call(&[])?;

    Ok(())
}
