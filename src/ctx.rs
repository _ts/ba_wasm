
use std::cell::RefCell;
use std::collections::HashMap;
use std::iter::FromIterator;
use crate::handle::{NetError, UriHandle, NetResult};

#[derive(Copy, Clone)]
pub struct WasiNetConfig {
    pub max_guest_buffer_len: u32
}

impl Default for WasiNetConfig {
    fn default() -> Self {
        WasiNetConfig {
            max_guest_buffer_len: 512
        }
    }
}

pub struct NetContext {
    pub(crate) handles: HashMap<String, Box<dyn UriHandle>>,
    pub(crate) config: WasiNetConfig,
}

pub struct WasiNetCtx {
    pub(crate) ctx: RefCell<NetContext>
}
//
// impl Borrow<str> for Box<dyn UriHandle> {
//     fn borrow(&self) -> &str {
//         self.alias()
//     }
// }

impl NetContext {
    pub fn new(handles: Vec<(String, Box<dyn UriHandle>)>, config: Option<WasiNetConfig>) -> Self {
        NetContext {
            handles: HashMap::from_iter(handles),
            config: config.unwrap_or(WasiNetConfig::default()),
        }
    }

    pub fn get_handle(&mut self, alias: &str) -> NetResult<&mut Box<dyn UriHandle>> {
        self.handles.get_mut(alias.into()).ok_or(NetError::AliasNotFound)
    }
}

impl WasiNetCtx {
    pub fn new(handles: Vec<(String, Box<dyn UriHandle>)>, config: Option<WasiNetConfig>) -> Self {
        WasiNetCtx {
            ctx: RefCell::new(NetContext::new(handles, config))
        }
    }
}