use wiggle::{GuestError, GuestErrorType, GuestPtr};

use crate::ctx::{WasiNetConfig, WasiNetCtx};
use crate::handle::*;

impl Into<types::NetConfig> for WasiNetConfig {
    fn into(self) -> types::NetConfig {
        types::NetConfig {
            max_buf_len: self.max_guest_buffer_len
        }
    }
}

wiggle::from_witx!({
    witx: ["witx/networking.witx"],
    ctx: WasiNetCtx,
    errors: { net_errno => NetError }
});

impl networking::Networking for WasiNetCtx {
    fn net_config(&self) -> NetResult<types::NetConfig> {
        Ok(self.ctx.borrow().config.into())
    }

    fn net_open(&self, alias: &GuestPtr<str>) -> NetResult<u64> {
        eprintln!("open");
        self.ctx.borrow_mut()
            .get_handle(&*alias.as_str()?)?
            .open()
    }

    fn net_read(&self, alias: &GuestPtr<str>, buf: &GuestPtr<[u8]>, peer: u64) -> NetResult<u32> {
        eprintln!("read");
        let max_len = self.ctx.borrow().config.max_guest_buffer_len;

        let mut buf = if buf.len() > max_len {
            buf.as_ptr().as_array(max_len).as_slice_mut()?
        } else {
            buf.as_slice_mut()?
        };

        self.ctx.borrow_mut()
            .get_handle(&*alias.as_str()?)?
            .read(&mut buf, peer)
    }

    fn net_write(&self, alias: &GuestPtr<str>, buf: &GuestPtr<[u8]>, peer: u64) -> NetResult<u32> {
        eprintln!("write");
        let max_len = self.ctx.borrow().config.max_guest_buffer_len;

        let buf = if buf.len() > max_len {
            buf.as_ptr().as_array(max_len).as_slice()?
        } else {
            buf.as_slice()?
        };

        self.ctx.borrow_mut()
            .get_handle(&*alias.as_str()?)?
            .write(&buf, peer)
    }

    fn net_close(&self, alias: &GuestPtr<str>, peer: u64) -> NetResult<()> {
        eprintln!("close");
        self.ctx.borrow_mut()
            .get_handle(&*alias.as_str()?)?
            .close(peer)?;
        Ok(())
    }
}

impl GuestErrorType for types::NetErrno {
    fn success() -> Self {
        Self::Ok
    }
}

impl From<GuestError> for NetError {
    fn from(_e: GuestError) -> Self {
        NetError::Other
    }
}

impl types::GuestErrorConversion for WasiNetCtx {
    fn into_net_errno(&self, e: GuestError) -> types::NetErrno {
        eprintln!("Guest error: {:?}", e);
        types::NetErrno::Other
    }
}

impl types::UserErrorConversion for WasiNetCtx {
    fn net_errno_from_net_error(&self, e: NetError) -> Result<types::NetErrno, String> {
        eprintln!("Host error: {:?}", e);
        match e {
            NetError::AliasNotFound => Ok(types::NetErrno::AliasNotFound),
            NetError::EndOfFile => Ok(types::NetErrno::EndOfFile),
            NetError::PeerNotFound => Ok(types::NetErrno::PeerNotFound),
            NetError::InvalidUri |
            NetError::InsufficientCapabilities |
            NetError::InvalidCapabilities |
            NetError::Other => Ok(types::NetErrno::Other),
        }
    }
}