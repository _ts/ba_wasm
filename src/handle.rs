use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::io::{Read, Write};
use std::io;
use std::net::{IpAddr, Shutdown, TcpListener, TcpStream};

use anyhow::Result;
use http::Uri;
use http::uri::{InvalidUri, Port};
use thiserror::Error;

#[derive(Error, Debug, PartialEq, Eq)]
pub enum NetError {
    #[error("invalid uri")]
    InvalidUri,
    #[error("insufficient capabilities")]
    InsufficientCapabilities,
    #[error("invalid capabilities")]
    InvalidCapabilities,
    #[error("end of file")]
    EndOfFile,
    #[error("alias not found")]
    AliasNotFound,
    #[error("peer not found")]
    PeerNotFound,
    #[error("unspecified")]
    Other,
}

pub type NetResult<T> = std::result::Result<T, NetError>;

#[derive(Copy, Clone)]
pub struct NetCapabilities {
    /// If true, the handle acts as a server accepting clients.
    /// Otherwise the handle connects to a server.
    pub listen: bool,

    pub read: bool,
    pub write: bool,
}

pub trait UriHandle {
    fn open(&mut self) -> NetResult<u64>;
    fn read(&mut self, buf: &mut [u8], peer_id: u64) -> NetResult<u32>;
    fn write(&mut self, buf: &[u8], peer_id: u64) -> NetResult<u32>;
    fn close(&mut self, peer_id: u64) -> NetResult<()>;

    fn capabilities(&self) -> &NetCapabilities;
}

/// A TCP socket listener. Handles one connection at the time.
pub struct TcpListenerUriHandle {
    listener: TcpListener,
    peer: Option<(TcpStream, u64)>,
    capabilities: NetCapabilities,
}

impl TcpListenerUriHandle {
    /// Opens a TCP listener accepting any TCP client.
    /// On this stage, it only allows URIs matching the pattern `raw://127.0.0.1:1234` or `raw://[::1]:1234`.
    pub fn new(uri: &str, capabilities: NetCapabilities) -> Result<Self> {
        let ip = match uri.parse::<Uri>() {
            Ok(uri) => {
                match (uri.scheme_str(), uri.host(), uri.port_u16(), uri.path(), uri.query()) {
                    (Some(s), Some(h), Some(p), "/", None) if s == "raw" => {
                        match h.parse::<IpAddr>() {
                            Ok(a) if a.is_loopback() => Ok(format!("{}:{}", h, p)),
                            _ => Err(NetError::InvalidUri)
                        }
                    },
                    _ => Err(NetError::InvalidUri)
                }
            },
            _ => Err(NetError::InvalidUri)
        }?;

        match capabilities {
            NetCapabilities { listen: false, .. } => Err(NetError::InvalidCapabilities),
            _ => Ok(())
        }?;

        Ok(TcpListenerUriHandle {
            listener: TcpListener::bind(ip)?,
            peer: None,
            capabilities,
        })
    }

    fn get_peer(&mut self) -> NetResult<(&mut TcpStream, u64)> {
        if let Some((ref mut peer, id)) = self.peer {
            Ok((peer, id))
        } else {
            Err(NetError::PeerNotFound)
        }
    }

    fn accept(&mut self) -> io::Result<u64> {
        if let Some((_, id)) = self.peer {
            Ok(id)
        } else {
            let peer = self.listener.accept().map(|(a, _b)| a)?;
            //peer.set_read_timeout(Some(Duration::from_millis(200)));
            //peer.set_write_timeout(Some(Duration::from_millis(200)));
            let addr = peer.peer_addr()?;
            let id = {
                let mut hasher = DefaultHasher::new();
                addr.ip().hash(&mut hasher);
                addr.port().hash(&mut hasher);

                hasher.finish()
            };
            self.peer.replace((peer, id));
            Ok(id)
        }
    }
}

impl From<io::Error> for NetError {
    fn from(e: io::Error) -> Self {
        eprintln!("io error: {:?}", e);
        match e.kind() {
            std::io::ErrorKind::UnexpectedEof => NetError::EndOfFile,
            _ => NetError::Other
        }
    }
}

impl UriHandle for TcpListenerUriHandle {
    fn open(&mut self) -> NetResult<u64> {
        match self.capabilities {
            NetCapabilities { read: false, .. } => Err(NetError::InsufficientCapabilities),
            _ => Ok(())
        }?;

        match self.peer {
            None => {}
            Some((_, peer_id)) => {
                // dispose any connection on a call to open before the connection is actually closed
                let _ = self.close(peer_id);
            }
        };

        match self.accept() {
            Ok(id) => Ok(id),
            _ => Err(NetError::AliasNotFound)
        }
    }

    fn read(&mut self, buf: &mut [u8], peer_id: u64) -> NetResult<u32> {
        match self.capabilities {
            NetCapabilities { read: false, .. } => Err(NetError::InsufficientCapabilities),
            _ => Ok(())
        }?;

        let len = match self.get_peer() {
            Ok((peer, id)) if id == peer_id => {
                Ok(peer.read(buf)?)
            }
            _ => Err(NetError::PeerNotFound)
        }?;

        if len == 0 {
            let _ = self.close(peer_id);
            Err(NetError::EndOfFile)?;
        }

        Ok(len as u32)
    }

    fn write(&mut self, buf: &[u8], peer_id: u64) -> NetResult<u32> {
        match self.capabilities {
            NetCapabilities { write: false, .. } => Err(NetError::InsufficientCapabilities),
            _ => Ok(())
        }?;

        match self.get_peer() {
            Ok((peer, id)) if id == peer_id => {
                Ok(peer.write(buf)? as u32)
            }
            _ => Err(NetError::PeerNotFound)
        }
    }

    fn close(&mut self, peer_id: u64) -> NetResult<()> {
        match self.get_peer() {
            Ok((peer, id)) if id == peer_id => {
                let r = peer.shutdown(Shutdown::Both);
                self.peer.take();
                r?;

                Ok(())
            }
            _ => Err(NetError::PeerNotFound)
        }
    }

    fn capabilities(&self) -> &NetCapabilities {
        &self.capabilities
    }
}

#[cfg(test)]
mod tests {
    use std::io::Write;
    use std::net::{Shutdown, TcpStream};
    use std::sync::{Arc, Barrier, Condvar, Mutex};
    use std::thread;
    use std::time::Duration;

    use anyhow::private::kind::TraitKind;
    use serial_test::serial;

    use crate::handle::{NetCapabilities, NetError, TcpListenerUriHandle, UriHandle};

    #[test]
    #[serial]
    fn test_io() {
        let client = thread::spawn(move || {
            let mut handle = TcpListenerUriHandle::new("raw://127.0.0.1:1234", NetCapabilities {
                listen: true,
                read: true,
                write: false,
            }).unwrap();

            assert!(handle.peer.is_none());

            let peer = handle.open().unwrap();
            assert!(handle.peer.is_some());

            {
                let (_, id) = handle.peer.as_ref().unwrap();
                assert_ne!(*id, 0);
            }

            let mut buf = vec![0; 128];
            let len = handle.read(&mut buf, peer).unwrap() as usize;

            assert_eq!("Hello Test!", String::from_utf8_lossy(&buf[..len]));

            assert_eq!(Some(NetError::EndOfFile), handle.read(&mut buf, peer).err());
            assert_eq!("Hello Test!", String::from_utf8_lossy(&buf[..len]));
            assert!(handle.peer.is_none());
        });

        thread::sleep(Duration::from_millis(1000));

        {
            let mut server_con = TcpStream::connect("127.0.0.1:1234").unwrap();

            server_con.write("Hello Test!".to_string().as_bytes());

            server_con.shutdown(Shutdown::Both).unwrap();
        }

        client.join().unwrap();
    }

    #[test]
    #[should_panic]
    #[serial]
    fn test_cap_listen() {
        TcpListenerUriHandle::new("raw://127.0.0.1:1234", NetCapabilities {
            listen: false,
            read: true,
            write: false,
        }).unwrap();
    }

    #[test]
    #[should_panic]
    #[serial]
    fn test_cap_open() {
        let mut handle = TcpListenerUriHandle::new("raw://127.0.0.1:1234", NetCapabilities {
            listen: true,
            read: false,
            write: false,
        }).unwrap();

        handle.open().unwrap();
    }

    #[test]
    #[serial]
    fn test_cap_write() {
        let client = thread::spawn(move || {
            let mut handle = TcpListenerUriHandle::new("raw://127.0.0.1:2345", NetCapabilities {
                listen: true,
                read: true,
                write: false,
            }).unwrap();

            let peer = handle.open().unwrap();
            let mut buf = vec![0; 128];

            assert!(handle.write(&mut buf, peer).err().is_some());
        });

        thread::sleep(Duration::from_millis(1000));

        {
            let mut server_con = TcpStream::connect("127.0.0.1:2345").unwrap();
        }

        client.join().unwrap();
    }
}
